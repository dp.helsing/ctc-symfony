PHP=php
CONSOLE=bin/console
BOWER=bower
COMPOSER=composer
WWW_USER=www-data
WWW_GROUP=www-data
DOCKER_COMPOSE=docker-compose
PHP_UNIT=vendor/bin/phpunit
PHP_CONTAINER=php_ctc_test_symfony
APT_GET=apt-get
CURL=curl

cc: cache mod

cc_dev: cache_dev mod

cache:
	$(PHP) $(CONSOLE) cache:clear --env=prod --no-debug --no-warmup

cache_dev:
	$(PHP) $(CONSOLE) cache:clear --env=dev --no-debug --no-warmup

mod:
	chown -R $(WWW_USER):$(WWW_GROUP) bin/
	chown -R $(WWW_USER):$(WWW_GROUP) config/
	chown -R $(WWW_USER):$(WWW_GROUP) public/
	chown -R $(WWW_USER):$(WWW_GROUP) src/
	chown -R $(WWW_USER):$(WWW_GROUP) vendor/

composer_update:
	$(APT_GET) update -yqq
	$(APT_GET) install git -yqq
	$(CURL) -sS https://getcomposer.org/installer | $(PHP) -- --install-dir=/usr/local/bin --filename=$(COMPOSER)
	$(COMPOSER) update  --no-progress --profile --prefer-dist

composer_install:
	$(APT_GET) update -yqq
	$(APT_GET) install git -yqq
	$(CURL) -sS https://getcomposer.org/installer | $(PHP) -- --install-dir=/usr/local/bin --filename=$(COMPOSER)
	$(COMPOSER) install  --no-progress --profile --prefer-dist

dup:
	$(DOCKER_COMPOSE) build
	$(DOCKER_COMPOSE) up -d

dstop:
	$(DOCKER_COMPOSE) stop

dps:
	$(DOCKER_COMPOSE) ps

drm:
	$(DOCKER) rm $($(DOCKER) ps -aq)

drmi:
	$(DOCKER) rmi $($(DOCKER) images -q)

dstart:
	$(DOCKER_COMPOSE) build && $(DOCKER_SYNC) start

dphp:
	$(DOCKER_COMPOSE) exec $(PHP_CONTAINER) bash

d: dstop dup dphp